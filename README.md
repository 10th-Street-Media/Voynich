# Voynich

Voynich is a software platform meant to make it easier for software projects to be localized into other languages. As the saying goes, "Necessity is the mother of invention," so Voynich was borne out of the need for a localization tool that was easy to install, configure, and free of charge. It is built on the Torty PHP framework and developed alongside other 10th Street Media projects.
